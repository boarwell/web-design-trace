// @ts-check

'use strict';
const signInTab = document.querySelector('.p-tab__sign-in');
const registerTab = document.querySelector('.p-tab__register');

const signInArea = document.getElementById('js-sign-in');
const registerArea = document.getElementById('js-register');

const toggleTab = () => {
  if (signInTab.classList.contains('u-current')) {
    signInTab.classList.replace('u-current', 'u-inactive');
    registerTab.classList.replace('u-inactive', 'u-current');
  } else {
    signInTab.classList.replace('u-inactive', 'u-current');
    registerTab.classList.replace('u-current', 'u-inactive');
  }
};

const toggleDisplay = () => {
  signInArea.classList.toggle('u-hidden');
  registerArea.classList.toggle('u-hidden');
};

const launchToggle = e => {
  if (e.target.classList.contains('u-current')) {
    return;
  }
  toggleDisplay();
  toggleTab();
};

signInTab.addEventListener('click', launchToggle);
registerTab.addEventListener('click', launchToggle);
