# 実装メモ

## ページ最下部にフッタを固定する

[このページ](https://www.tipdip.jp/tips_posts/production/2213/)が参考になった。

考え方は以下の通り。

- `body`の`min-height`を`100vh`にして画面いっぱいに`body`が占めるようにする

- `footer`の`margin-top`を`auto`にして常に最下部に表示されるようにする
